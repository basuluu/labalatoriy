
/*
* Задание #6
* Автор:
*
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main(int argc, char * argv[])
{
    COUNTRY * list;

    /* Загрузка списка */
    list = load();

    if (argc < 2)
    {
        printf("Ошибка формата: country <команда> <параметры>\n");
    }
    else
    {
        if (!strcmp(argv[1], "add"))
        {
            if (argc != 5)
            {
                printf("Ошибка формата! add <название> <население> <площадь>\n");
            }
            else
            {
                int err = add(&list, argv[2], atoi(argv[3]), atoi(argv[4]));
                if (err != 0) {
                    printf("ошибка при выполнении команды\n");
                }
            }
        }
        else if (!strcmp(argv[1], "delete"))
        {
            if (argc != 3)
            {
                printf("ошибка формата!: country delete <название>\n");
            }
            else
            {
                COUNTRY* p = find(list, argv[2]);
                if (!p)
                {
                    printf("названия нет в базе данных!\n");
                }
                else
                {
                    delete(&list, p);
                }
            }
        }
        else if (!strcmp(argv[1], "dump"))
        {
            if (argc > 3)
            {
                printf("format: country dump <key>\n");
            }
            else
            {
                if (argc == 2)
                {
                    dump(list);
                }
                else
                {
                    if (!strcmp(argv[2], "-n"))
                    {
                        int err = sort_by_name(&list);
                        dump(list);
                        if (err != 0)
                        {
                            printf("ошибка при выполнении команды\n");
                        }
                    }
                    else if (!strcmp(argv[2], "-a"))
                    {
                        int err = sort_by_area(&list);
                        dump(list);
                        if (err != 0)
                        {
                            printf("ошибка при выполнении команды\n");
                        }
                    }
                    else if (!strcmp(argv[2], "-p"))
                    {
                        int err = sort_by_population(&list);
                        dump(list);
                        if (err != 0)
                        {
                            printf("ошибка при выполнении команды\n");
                        }
                    }
                    else
                    {
                        printf("ошибка формата!: country dump <ключ>\n");
                    }
                }
            }
        }
        else if (!strcmp(argv[1], "view"))
        {
            if (argc != 3)
            {
                printf("ошибка формата!: country view <название>\n");
            }
            else
            {
                COUNTRY* p = find(list, argv[2]);
                if (!p)
                {
                    printf("название отсутсвует в базе данных!\n");
                }
                else
                {
                    print_country(p);
                }
            }
        }
        else if (!strcmp(argv[1], "count"))
        {
            if (argc != 2)
            {
                printf("ошибка формата!: country count\n");
            }
            else
            {
                printf("%d\n", count(list));
            }
        }
        else
        {
            printf("команды не существует!\n");
            printf("Список доступных команд:!\n");
            printf("country add <название> <население> <площадь>\n");
            printf("country delete <название страны>\n");
            printf("country view <название страны>\n");
            printf("country count\n");
            printf("country dump\ncountry dump -n\ncountry dump -a\ncountry dump -p\n");
            return 1;
        }
    }

    save(list);
    /* Удаление списка из динамической памяти */
    clear(list);

    return 0;

}

