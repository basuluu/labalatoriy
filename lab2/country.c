
/*
* Задание #6
* Автор:
*
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "map.h"

void clear_name(char * name){
    while (*name != '\0')
    {
        if (*name == '_')
        {
            *name = ' ';
        }
        ++name;
    }
}

int main(int argc, char * argv[])
{

    COUNTRY** map;
    map = map_load();

    char cmd[16];
    char name[256];
    int population;
    int area;

    while (1) {
        scanf("%s", cmd);
        if (strcmp(cmd, "add") == 0) {
            scanf("%s", name);
            scanf("%d", &population);
            scanf("%d", &area);

            clear_name(name);
            map_add(map, name, population, area);
        } 
        else if (strcmp(cmd, "delete") == 0) {
            scanf("%s", name);
            clear_name(name);

            map_delete(map, name);
        }
        else if (!strcmp(cmd, "view")) {
            scanf("%s", name);
            clear_name(name);

            COUNTRY* p = map_find(map, name);
            if (!p)
            {
                printf("названия нет в базе данных!\n");
            }
            else
            {
                print_country(p);
            }
        }
        else if (!strcmp(cmd, "dump")) {
            map_dump(map);
        }
        else if (!strcmp(cmd, "save")) {
            map_save(map);
        }
        else if (!strcmp(cmd, "quit")) {
            break;
        }
        else {
            printf("команды не существует!\n");
            printf("Список доступных команд:!\n");
            printf("add <название> <население> <площадь>\n");
            printf("delete <название страны>\n");
            printf("view <название страны>\n");
            printf("dump\n");
            printf("save\n");
            printf("quit\n");
            return 1;
        }
    }

    map_save(map);
    map_clear(map);

    return 0;

}

